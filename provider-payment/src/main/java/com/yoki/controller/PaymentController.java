package com.yoki.controller;

/**
 * @program: microservices-demo
 * @author: yoki
 * @create: 2023-07-20 21:04
 */

import com.yoki.pojo.CommonResult;
import com.yoki.pojo.Payment;
import com.yoki.serice.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/*
 * 提供restful服务  供其他服务调用
 *
 * */
@RestController
@Slf4j
class PaymentController {
    @Autowired
    private PaymentService paymentService;

    //注入服务发现的注解
    @Autowired
    private DiscoveryClient discoveryClient;

    //获取服务信息
    @GetMapping("/payment/discovery")
    public Object discovery() {
        List<String> services = discoveryClient.getServices();
        for (String s : services) {
            log.info("********注册到eureka中的服务中有:" + services);
        }
        List<ServiceInstance> instances = discoveryClient.getInstances("MCROSERVICE-PAYMENT");
        for (ServiceInstance s : instances) {
            log.info("当前服务的实例有" + s.getServiceId() + "\t" + s.getHost() + "\t" + s.getPort() + "\t" + s.getUri());
        }
        return this.discoveryClient;
    }

    @PostMapping("/payment/create")
    public CommonResult create(@RequestBody Payment dept) {
        int id = paymentService.create(dept);
        log.info("***************插入成功*******" + id);
        if (id > 0) {
            return new CommonResult(200, "插入数据库成功" + id);
        } else {
            return new CommonResult(444, "插入数据库失败", null);
        }
    }

    @GetMapping("/payment/get/{id}")
    public CommonResult queryById(@PathVariable("id") Long id) {
        Payment payment = paymentService.queryById(id);
        log.info("***************查询成功*********" + payment);
        if (payment != null) {
            return new CommonResult(200, "查询成功" ,payment);
        } else {
            return new CommonResult(444, "查询失败", null);
        }
    }
}
