package com.yoki.dao;

import com.yoki.pojo.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @program: microservices-demo
 * @author: yoki
 * @create: 2023-07-20 20:58
 */
@Mapper
@Repository
public interface PaymentDao {
    int create(Payment payment);

    Payment queryById(@Param("id") long id);

}
